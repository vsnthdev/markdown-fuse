﻿namespace Markdown_Fuse.CustomComponents
{
    public partial class StartPage : IDEFuse.CustomComponents.StartPage
    {
        #region Custom Variables

        // The mainWindow global
        private MainWindow mainWindow;

        #endregion

        #region Initialization

        public StartPage(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;

            ComponentTheme = mainWindow.appSettings.Theme;
            picAppIcon.Image = Properties.Resources.AppIcon;
            lblAppVersion.Text = "Version: " + mainWindow.appSettings.GetAppVersion();
        }

        #endregion

        #region Control Events

        private void btnCreateNewDocument_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            mainWindow.CreateNewDocument();

            // If CloseStartPageAfterLoaded is true, then close itself
            if (mainWindow.appSettings.CloseStartPageAfterLoaded == true)
            {
                Close();
            }
        }

        private void btnOpenDocument_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            mainWindow.OpenNewDocument();

            // If CloseStartPageAfterLoaded is true, then close itself
            if (mainWindow.appSettings.CloseStartPageAfterLoaded == true)
            {
                Close();
            }
        }

        #endregion
    }
}
