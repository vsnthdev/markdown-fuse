﻿using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Markdown_Fuse.CustomComponents
{
    public partial class MarkdownEditor : IDEFuse.CustomComponents.CodeEditor
    {
        #region Custom Properties

        public bool SaveSafe { get; internal set; } = true;

        #endregion

        #region Custom Variables

        // The global mainWindow object
        private MainWindow mainWindow;

        // Markdown File variables
        public string MarkdownFilePath;
        private string FileNameWithExtension;
        private string FileNameWithoutExtension;
        private string FileExtension;

        // Text encoding
        private Encoding textEncoding;

        #endregion

        #region Initialization

        public MarkdownEditor(MainWindow mainWindow, string MarkdownFilePath)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.MarkdownFilePath = MarkdownFilePath;

            // Get the encoding string from AppSettings and set the encoding accordingly
            switch (mainWindow.appSettings.FileEncoding)
            {
                case "ASC-II":
                    textEncoding = Encoding.ASCII;
                    break;
                case "BigEndianUnicode":
                    textEncoding = Encoding.BigEndianUnicode;
                    break;
                case "Unicode":
                    textEncoding = Encoding.Unicode;
                    break;
                case "UTF-7":
                    textEncoding = Encoding.UTF7;
                    break;
                case "UTF-8":
                    textEncoding = Encoding.UTF8;
                    break;
                case "UTF-32":
                    textEncoding = Encoding.UTF32;
                    break;
                default:
                    textEncoding = Encoding.Default;
                    break;
            }

            ComponentTheme = mainWindow.appSettings.Theme;

            if (MarkdownFilePath == string.Empty)
            {
                Tag = "untitled";
            } else
            {
                FileNameWithExtension = Path.GetFileName(MarkdownFilePath);
                FileNameWithoutExtension = Path.GetFileNameWithoutExtension(MarkdownFilePath);
                FileExtension = Path.GetExtension(MarkdownFilePath);
            }
        }

        private void MarkdownEditor_Load(object sender, System.EventArgs e)
        {
            if (MarkdownFilePath == string.Empty)
            {
                Text = TabText = "New Document";
            } else
            {
                Text = TabText = FileNameWithoutExtension;
                Icon = Properties.Resources.FileOK_16x;

                LoadFile(false);
            }

            // Remove some shortcut keys given by default by ScintillaNET
            Editor.ClearCmdKey(Keys.Control | Keys.S);
            Editor.ClearCmdKey(Keys.Control | Keys.Shift | Keys.C);
        }

        #endregion

        #region Custom Methods

        public void LoadFile(bool ClearBeforeLoading)
        {
            if (ClearBeforeLoading == true)
                Editor.ClearAll();

            // Variable that reads all the lines in the file
            var lines = File.ReadLines(MarkdownFilePath);

            // Add each line into the Editor Component
            foreach (string line in lines)
            {
                Editor.Text = Editor.Text.Insert(Editor.TextLength, line + "\n");
            }

            // Set the cursor to the end
            Editor.SelectionStart = Editor.TextLength;

            // Clear all the undo methods
            Editor.EmptyUndoBuffer();

            // Set the icon and refresh the parent dock panel
            Icon = Properties.Resources.FileOK_16x;
            DockHandler.DockPanel.Refresh();

            // Disable the save menu to prevent unnecessary disk writes
            mainWindow.saveToolStripMenuItem.Enabled = false;

            // Set the SaveSafe to true
            SaveSafe = true;
        }

        public void SaveFile()
        {
            // Check if it is a new document or has a file path
            if (MarkdownFilePath == string.Empty)
            {
                // Prepare the Filter from supported extensions
                string FilterExtensions = string.Empty;
                foreach (string extension in mainWindow.SupportedExtensions)
                {
                    FilterExtensions = FilterExtensions + "*" + extension + ";";
                }
                FilterExtensions = FilterExtensions.Remove(FilterExtensions.Length - 1);

                // Create a new save file
                SaveFileDialog saveFileDialog = new SaveFileDialog();

                // Set the title
                saveFileDialog.Title = "Save with Markdown Fuse";

                // Set the filter
                saveFileDialog.Filter = "Markdown Files|" + FilterExtensions;

                // Set the filter index
                saveFileDialog.FilterIndex = 0;

                // Set the initial directory
                saveFileDialog.InitialDirectory = mainWindow.directoryManager.DocumentDirectory;

                // Show the dialog and collect the interaction
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    // Set the Markdown file path
                    MarkdownFilePath = saveFileDialog.FileName.ToString();

                    // Update all other variables
                    FileNameWithExtension = Path.GetFileName(saveFileDialog.FileName.ToString());
                    FileNameWithoutExtension = Path.GetFileNameWithoutExtension(saveFileDialog.FileName.ToString());
                    FileExtension = Path.GetExtension(saveFileDialog.FileName.ToString());

                    // Set the tag
                    Tag = mainWindow.GetSanitizedSlug(FileNameWithoutExtension);

                    // Set the title and icon
                    Text = TabText = FileNameWithoutExtension;
                    Icon = Properties.Resources.FileOK_16x;

                    // Write the file to disk
                    File.WriteAllText(saveFileDialog.FileName.ToString(), Editor.Text.ToString(), textEncoding);

                    // Set the save point
                    Editor.SetSavePoint();
                }
            } else
            {
                // Write the file to the disk
                File.WriteAllText(MarkdownFilePath, Editor.Text.ToString(), textEncoding);

                // Set the save point
                Editor.SetSavePoint();
            }

            // Show the application status
            mainWindow.SetAppStatus("Saved at: " + MarkdownFilePath + " (" + mainWindow.appSettings.FileEncoding.ToString() + ")");

            // Disable the save menu to prevent unnecessary disk writes
            mainWindow.saveToolStripMenuItem.Enabled = false;
        }

        public void SaveAsFile()
        {
            // Prepare the Filter from supported extensions
            string FilterExtensions = string.Empty;
            foreach (string extension in mainWindow.SupportedExtensions)
            {
                FilterExtensions = FilterExtensions + "*" + extension + ";";
            }
            FilterExtensions = FilterExtensions.Remove(FilterExtensions.Length - 1);

            // Create a new save file
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            // Set the title
            saveFileDialog.Title = "Save with Markdown Fuse";

            // Set the filter
            saveFileDialog.Filter = "Markdown Files|" + FilterExtensions;

            // Set the filter index
            saveFileDialog.FilterIndex = 0;

            // Set the initial directory
            saveFileDialog.InitialDirectory = mainWindow.directoryManager.DocumentDirectory;

            // Show the dialog and collect the interaction
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Set the Markdown file path
                MarkdownFilePath = saveFileDialog.FileName.ToString();

                // Update all other variables
                FileNameWithExtension = Path.GetFileName(saveFileDialog.FileName.ToString());
                FileNameWithoutExtension = Path.GetFileNameWithoutExtension(saveFileDialog.FileName.ToString());
                FileExtension = Path.GetExtension(saveFileDialog.FileName.ToString());

                // Set the tag
                Tag = mainWindow.GetSanitizedSlug(FileNameWithoutExtension);

                // Set the title and icon
                Text = TabText = FileNameWithoutExtension;
                Icon = Properties.Resources.FileOK_16x;

                // Write the file to disk
                File.WriteAllText(saveFileDialog.FileName.ToString(), Editor.Text.ToString(), Encoding.UTF8);

                // Set the save point
                Editor.SetSavePoint();

                // Disable the save menu to prevent unnecessary disk writes
                mainWindow.saveToolStripMenuItem.Enabled = false;
            }
        }

        #endregion

        #region Control Events

        private void Editor_SavePointReached(object sender, System.EventArgs e)
        {
            // Set the file icon
            if (MarkdownFilePath == string.Empty)
            {
                Icon = Properties.Resources.NewFile_16x1;
            } else
            {
                Icon = Properties.Resources.FileOK_16x;
            }

            DockHandler.DockPanel.Refresh();

            // Disable the save menu to prevent unnecessary disk writes
            mainWindow.saveToolStripMenuItem.Enabled = false;

            // Set the SaveSafe to true
            SaveSafe = true;
        }

        private void Editor_SavePointLeft(object sender, System.EventArgs e)
        {
            // Set the file icon
            Icon = Properties.Resources.FileWarning_16x;
            DockHandler.DockPanel.Refresh();

            // Enable the save menu to allow the user to save again
            mainWindow.saveToolStripMenuItem.Enabled = true;

            // Set SaveSafe to false
            SaveSafe = false;
        }

        private void Editor_UpdateUI(object sender, ScintillaNET.UpdateUIEventArgs e)
        {
            // Remove the dot if cannot undo
            if (Editor.CanUndo == false)
            {
                mainWindow.undoToolStripMenuItem.Enabled = false;
                Icon = Properties.Resources.FileOK_16x;
                DockHandler.DockPanel.Refresh();
            }
            else
            {
                mainWindow.undoToolStripMenuItem.Enabled = true;
            }

            // Enable or disable Redo accordingly
            if (Editor.CanRedo == true)
            {
                mainWindow.redoToolStripMenuItem.Enabled = true;
            }
            else
            {
                mainWindow.redoToolStripMenuItem.Enabled = false;
            }

            // Check if selected text changed
            if (Editor.SelectedText != string.Empty)
            {
                mainWindow.cutToolStripMenuItem.Enabled = true;
                mainWindow.copyToolStripMenuItem.Enabled = true;
                mainWindow.copyAsHTMLToolStripMenuItem.Enabled = true;
            }
            else
            {
                mainWindow.cutToolStripMenuItem.Enabled = false;
                mainWindow.copyToolStripMenuItem.Enabled = false;
                mainWindow.copyAsHTMLToolStripMenuItem.Enabled = false;
            }

            // Check if delete can be pressed anymore
            if (Editor.SelectionStart == Editor.TextLength)
            {
                mainWindow.deleteToolStripMenuItem.Enabled = false;
            }
            else
            {
                mainWindow.deleteToolStripMenuItem.Enabled = true;
            }
        }

        private void MarkdownEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (SaveSafe == false)
            {
                DialogResult dialogResult = MessageBox.Show("Unsaved changes will be discarded. Are you sure you want to close?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dialogResult == DialogResult.No)
                {
                    e.Cancel = true;
                } else
                {
                    e.Cancel = false;
                }
            }
        }

        #endregion
    }
}
