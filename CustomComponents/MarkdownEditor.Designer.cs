﻿namespace Markdown_Fuse.CustomComponents
{
    partial class MarkdownEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Editor
            // 
            this.Editor.CaretForeColor = System.Drawing.Color.Gainsboro;
            this.Editor.CaretWidth = 2;
            this.Editor.SavePointLeft += new System.EventHandler<System.EventArgs>(this.Editor_SavePointLeft);
            this.Editor.SavePointReached += new System.EventHandler<System.EventArgs>(this.Editor_SavePointReached);
            this.Editor.UpdateUI += new System.EventHandler<ScintillaNET.UpdateUIEventArgs>(this.Editor_UpdateUI);
            // 
            // MarkdownEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1071, 656);
            this.Name = "MarkdownEditor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MarkdownEditor_FormClosing);
            this.Load += new System.EventHandler(this.MarkdownEditor_Load);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
