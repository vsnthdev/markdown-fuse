﻿using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using IDEFuse.LogManager;
using Markdown_Fuse.CustomComponents;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using System.Diagnostics;
using ScintillaNET;
using Markdown_Fuse.CustomClasses;
using CommonMark;

namespace Markdown_Fuse
{
    public partial class MainWindow : Form
    {
        #region Initialization

        public MainWindow()
        {
            InitializeComponent();

            // Load the directory manager
            directoryManager = new DirectoryManager(Application.ProductName);

            // Load the log manager
            logManager = new LogManager(directoryManager.LogDirectory, Application.ProductName);

            // Load the settings manager, which will initialize the appSettings
            settingsManager = new SettingsManager("7F0304FB85A5BFE8E18F1174C1B5CC97", logManager);

            // Load the Settings
            appSettings = settingsManager.LoadFile(Path.Combine(directoryManager.SettingsDirectory, "preferences.config"));

            // Load the Markdown Engine
            markdownEngine = new MarkdownEngine();

            // Set this as MIDI container so CustomComponents can sit
            IsMdiContainer = true;

            // [TEMP]: Set the Light or Dark Theme
            SetTheme(appSettings.Theme);

            // Build CustomComponents Required for this instance
            BuildCustomComponents(appSettings.Theme);

            // Set the application version in StartPage
            startPage.lblAppVersion.Text = "Version: " + appSettings.GetAppVersion();

            // Set the icon in the StartPage
            startPage.picAppIcon.Image = Properties.Resources.AppIcon;
        }

        private void MainWindow_Load(object sender, System.EventArgs e)
        {
            // Enable or disable the tool bar and status bar by checking the AppSettings
            toolbarToolStripMenuItem.Checked = appSettings.ShowToolbar;
            statusbarToolStripMenuItem.Checked = appSettings.ShowStatusbar;
            AppToolBar.Visible = appSettings.ShowToolbar;
            AppStatusBar.Visible = appSettings.ShowStatusbar;

            // Add the StartPage to AppDock
            startPage.Show(AppDock);
        }

        #endregion

        #region Custom Variables

        private bool isFullscreen = false;
        private FormWindowState previousWindowState = FormWindowState.Maximized;
        public List<string> SupportedExtensions = new List<string>
        {
            ".markdown",
            ".mdown",
            ".mkdn",
            ".md",
            ".mkd",
            ".mdwn",
            ".mdtxt",
            ".mdtext"
        };

        #endregion

        #region Components Initialization

        public StartPage startPage;

        private void BuildCustomComponents(int Theme)
        {
            startPage = new StartPage(this);
        }

        #endregion

        #region Classes Initialization

        // Create a new directory manager
        public DirectoryManager directoryManager;

        // Create a new log manager
        public LogManager logManager;

        // Create a settings manager
        public SettingsManager settingsManager;

        // Get the generated settings
        public AppSettings appSettings;

        // Prepare a Markdown Engine
        public MarkdownEngine markdownEngine;

        #endregion

        #region ThemeManager

        // Light Theme = 0
        // Dark Theme  = 1
        public void SetTheme(int themeStyle)
        {
            switch (themeStyle)
            {
                case 0:
                    // Set the AppDock theme
                    AppDock.Theme = LightTheme;

                    // Set the AppMenuBar, AppToolBar theme
                    ToolStripRenderer.SetStyle(AppMenuBar, VisualStudioToolStripExtender.VsVersion.Vs2015, LightTheme);
                    ToolStripRenderer.SetStyle(AppToolBar, VisualStudioToolStripExtender.VsVersion.Vs2015, LightTheme);
                    break;
                case 1:
                    // Set the AppDock theme
                    AppDock.Theme = DarkTheme;

                    // Set the AppMenuBar, AppToolBar theme
                    ToolStripRenderer.SetStyle(AppMenuBar, VisualStudioToolStripExtender.VsVersion.Vs2015, DarkTheme);
                    AppMenuBar.BackColor = Color.FromArgb(255, 45, 45, 48);
                    ToolStripRenderer.SetStyle(AppToolBar, VisualStudioToolStripExtender.VsVersion.Vs2015, DarkTheme);
                    break;
            }
        }

        #endregion

        #region Control Events

        #region FileMenu

        private void newToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            CreateNewDocument();
        }

        private void openToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            OpenNewDocument();
        }

        private void reloadFromDiskToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Get the current position inside Scintilla Editor component in active editor
            int currentPos = markdownEditor.Editor.SelectionStart;

            // Reload the file
            markdownEditor.LoadFile(true);

            // Put back the caret in the position where the user was before reloading
            markdownEditor.Editor.SelectionStart = currentPos;

            // Show the status as the file has been reloaded
            SetAppStatus("Document Reloaded from Disk.");
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            markdownEditor.SaveFile();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            markdownEditor.SaveAsFile();
        }

        private void saveAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the saved files count
            int SavedFilesCount = 0;

            // Get the totally checked documents count
            int TotalChecked = 0;

            // Loop through all the documents
            foreach (DockContent dockContent in AppDock.Contents)
            {
                // Try casting those as MarkdownEditor to see if it actually is a MarkdownEditor
                try
                {
                    // Convert the generic DockContent object into a MarkdownEditor
                    MarkdownEditor markdownEditor = (MarkdownEditor)dockContent;

                    // As there is no exception yet, count the total documents
                    TotalChecked++;

                    // Check if the document was modified or the undo buffer is clean
                    if (markdownEditor.Editor.CanUndo == true)
                    {
                        // Give it the save command
                        markdownEditor.SaveFile();

                        // Count the document as saved one
                        SavedFilesCount++;
                    }
                }
                catch
                {
                    // Simply do nothing to non-editor components and continue the foreach loop
                    continue;
                }
            }

            // Finally, show the message in the AppStatus
            SetAppStatus("Saved: " + SavedFilesCount.ToString() + " of " + TotalChecked.ToString() + " documents. (UTF-8)");
        }

        private void locateInExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Locate the document in Windows Explorer
            Process.Start("explorer.exe", "/select," + markdownEditor.MarkdownFilePath);
        }

        private void closeDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            DockContent dockContent = (DockContent)AppDock.ActiveContent;

            // Close it
            dockContent.Close();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Simply exit the application
            Application.Exit();
        }

        #endregion

        #region Edit Menu

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Send the undo signal
            markdownEditor.Editor.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Send the redo signal
            markdownEditor.Editor.Redo();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Copy the selected text into clipboard
            Clipboard.SetText(markdownEditor.Editor.SelectedText);

            // Show the user that text length
            SetAppStatus("Cut " + markdownEditor.Editor.SelectedText.Length.ToString() + " characters to clipboard.");

            // Remove the selected characters from the Editor
            markdownEditor.Editor.ExecuteCmd(Command.DeleteBack);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Copy the selected text into clipboard
            Clipboard.SetText(markdownEditor.Editor.SelectedText);

            // Show the user that text length
            SetAppStatus("Copied " + markdownEditor.Editor.SelectedText.Length.ToString() + " characters to clipboard.");
        }

        private void copyAsHTMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Compile the Markdown Text and copy it
            Clipboard.SetText(CommonMarkConverter.Convert(markdownEditor.Editor.Text));
        }

        private void copyDocumentAsHTMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Compile the Markdown and copy the resultant HTML
            Clipboard.SetText(markdownEngine.RenderMarkdown(Path.GetFileNameWithoutExtension(markdownEditor.MarkdownFilePath), markdownEditor.Editor.Text));
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Append the clipboard text into the editor
            markdownEditor.Editor.AddText(Clipboard.GetText());
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Move the caret one character right
            markdownEditor.Editor.ExecuteCmd(Command.CharRight);

            // Remove the left character now
            markdownEditor.Editor.ExecuteCmd(Command.DeleteBack);
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Get the active editor
            MarkdownEditor markdownEditor = (MarkdownEditor)AppDock.ActiveContent;

            // Send the select all signal
            markdownEditor.Editor.ExecuteCmd(Command.SelectAll);
        }

        #endregion

        #region View Menu

        private void toolbarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (appSettings.ShowToolbar == true)
            {
                toolbarToolStripMenuItem.Checked = false;
                AppToolBar.Visible = false;
                appSettings.ShowToolbar = false;
            } else
            {
                toolbarToolStripMenuItem.Checked = true;
                AppToolBar.Visible = true;
                appSettings.ShowToolbar = true;
            }
        }

        private void statusbarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (appSettings.ShowStatusbar == true)
            {
                statusbarToolStripMenuItem.Checked = false;
                AppStatusBar.Visible = false;
                appSettings.ShowStatusbar = false;
            } else
            {
                statusbarToolStripMenuItem.Checked = true;
                AppStatusBar.Visible = true;
                appSettings.ShowStatusbar = true;
            }
        }

        private void fullscreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isFullscreen == false)
            {
                // Enter full screen
                isFullscreen = true;
                previousWindowState = WindowState;
                WindowState = FormWindowState.Normal;
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
            } else
            {
                // Exit full screen
                WindowState = previousWindowState;
                FormBorderStyle = FormBorderStyle.Sizable;
                isFullscreen = false;
            }
        }

        #endregion

        private void AppDock_ActiveContentChanged(object sender, System.EventArgs e)
        {
            DockContent activeContent = (DockContent)AppDock.ActiveContent;

            try
            {
                switch (activeContent.Tag.ToString())
                {
                    case "start_page":
                        reloadFromDiskToolStripMenuItem.Enabled = false;
                        saveToolStripMenuItem.Enabled = false;
                        saveAsToolStripMenuItem.Enabled = false;
                        saveAllToolStripMenuItem.Enabled = false;
                        locateInExplorerToolStripMenuItem.Enabled = false;
                        undoToolStripMenuItem.Enabled = false;
                        redoToolStripMenuItem.Enabled = false;
                        cutToolStripMenuItem.Enabled = false;
                        copyToolStripMenuItem.Enabled = false;
                        copyAsHTMLToolStripMenuItem.Enabled = false;
                        copyDocumentAsHTMLToolStripMenuItem.Enabled = false;
                        pasteToolStripMenuItem.Enabled = false;
                        deleteToolStripMenuItem.Enabled = false;
                        selectAllToolStripMenuItem.Enabled = false;
                        break;
                    case "untitled":
                        reloadFromDiskToolStripMenuItem.Enabled = false;
                        saveToolStripMenuItem.Enabled = true;
                        saveAsToolStripMenuItem.Enabled = false;
                        saveAllToolStripMenuItem.Enabled = true;
                        locateInExplorerToolStripMenuItem.Enabled = false;
                        selectAllToolStripMenuItem.Enabled = true;
                        copyDocumentAsHTMLToolStripMenuItem.Enabled = true;

                        // Check if there is a something in the clipboard
                        if (Clipboard.ContainsText() == true)
                        {
                            pasteToolStripMenuItem.Enabled = true;
                        } else
                        {
                            pasteToolStripMenuItem.Enabled = false;
                        }

                        // Try treating the current document as a MarkdownEditor
                        MarkdownEditor markdownEditor = (MarkdownEditor)activeContent;
                        if (markdownEditor.Editor.CanUndo)
                        {
                            undoToolStripMenuItem.Enabled = true;
                        } else
                        {
                            undoToolStripMenuItem.Enabled = false;
                        }

                        if (markdownEditor.Editor.CanRedo)
                        {
                            redoToolStripMenuItem.Enabled = true;
                        } else
                        {
                            redoToolStripMenuItem.Enabled = false;
                        }
                        if (markdownEditor.Editor.SelectedText != string.Empty)
                        {
                            cutToolStripMenuItem.Enabled = true;
                            copyToolStripMenuItem.Enabled = true;
                            copyAsHTMLToolStripMenuItem.Enabled = true;
                        } else
                        {
                            cutToolStripMenuItem.Enabled = false;
                            copyToolStripMenuItem.Enabled = false;
                            copyAsHTMLToolStripMenuItem.Enabled = false;
                        }
                        if (markdownEditor.Editor.SelectionStart == markdownEditor.Editor.TextLength)
                        {
                            deleteToolStripMenuItem.Enabled = false;
                        } else
                        {
                            deleteToolStripMenuItem.Enabled = true;
                        }

                        break;
                    default:
                        reloadFromDiskToolStripMenuItem.Enabled = true;
                        selectAllToolStripMenuItem.Enabled = true;
                        copyDocumentAsHTMLToolStripMenuItem.Enabled = true;

                        // Check if there is a something in the clipboard
                        if (Clipboard.ContainsText() == true)
                        {
                            pasteToolStripMenuItem.Enabled = true;
                        }
                        else
                        {
                            pasteToolStripMenuItem.Enabled = false;
                        }

                        // Try treating the current document as a MarkdownEditor
                        try
                        {
                            MarkdownEditor markdownEditord = (MarkdownEditor)activeContent;
                            if (markdownEditord.Editor.CanUndo)
                            {
                                saveToolStripMenuItem.Enabled = true;
                                undoToolStripMenuItem.Enabled = true;
                            } else
                            {
                                saveToolStripMenuItem.Enabled = false;
                                undoToolStripMenuItem.Enabled = false;
                            }
                            if (markdownEditord.Editor.CanRedo)
                            {
                                redoToolStripMenuItem.Enabled = true;
                            } else
                            {
                                redoToolStripMenuItem.Enabled = false;
                            }
                            if (markdownEditord.Editor.SelectionStart == markdownEditord.Text.Length)
                            {
                                deleteToolStripMenuItem.Enabled = false;
                            }
                            else
                            {
                                deleteToolStripMenuItem.Enabled = true;
                            }
                            if (markdownEditord.Editor.SelectedText == string.Empty)
                            {
                                cutToolStripMenuItem.Enabled = false;
                                copyToolStripMenuItem.Enabled = false;
                                copyAsHTMLToolStripMenuItem.Enabled = false;
                            } else
                            {
                                cutToolStripMenuItem.Enabled = true;
                                copyToolStripMenuItem.Enabled = true;
                                copyAsHTMLToolStripMenuItem.Enabled = true;
                            }
                        } catch
                        {
                            saveToolStripMenuItem.Enabled = false;
                            undoToolStripMenuItem.Enabled = false;
                            redoToolStripMenuItem.Enabled = false;
                            deleteToolStripMenuItem.Enabled = false;
                        }

                        saveAsToolStripMenuItem.Enabled = true;
                        saveAllToolStripMenuItem.Enabled = true;
                        locateInExplorerToolStripMenuItem.Enabled = true;
                        break;
                }
            }
            catch { }
        }

        private void AppDock_ContentRemoved(object sender, DockContentEventArgs e)
        {
            if (AppDock.Contents.Count == 0)
            {
                Application.Exit();
            }
        }

        private void tmrStatusMessages_Tick(object sender, EventArgs e)
        {
            lblAppStatus.Text = "Ready";
            tmrStatusMessages.Stop();
        }

        #endregion

        #region Custom Methods

        public void CreateNewDocument()
        {
            bool NewTagAlreadyExists = false;
            foreach (DockContent dockContent in AppDock.Contents)
            {
                if ((string)dockContent.Tag == "untitled")
                {
                    NewTagAlreadyExists = true;
                    dockContent.Select();
                }
            }

            if (NewTagAlreadyExists == false)
            {
                MarkdownEditor markdownEditor = new MarkdownEditor(this, string.Empty);
                markdownEditor.Show(AppDock);
            }
        }

        public void OpenNewDocument()
        {
            string FilterExtensions = string.Empty;
            foreach (string extension in SupportedExtensions)
            {
                FilterExtensions = FilterExtensions + "*" + extension + ";";
            }
            FilterExtensions = FilterExtensions.Remove(FilterExtensions.Length -1);

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open in Markdown Fuse";
            openFileDialog.Filter = "Markdown Files|" + FilterExtensions;
            openFileDialog.FilterIndex = 0;

            // [TODO]: Enable opening multiple files in maybe the next versions
            openFileDialog.Multiselect = false;

            openFileDialog.InitialDirectory = directoryManager.DocumentDirectory;
            
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string FileNameWithoutExtension = Path.GetFileNameWithoutExtension(openFileDialog.FileName.ToString());
                string slug = GetSanitizedSlug(FileNameWithoutExtension);

                bool TagAlreadyExists = false;
                foreach (DockContent dockContent in AppDock.Contents)
                {
                    if ((string)dockContent.Tag == slug)
                    {
                        dockContent.Select();
                        TagAlreadyExists = true;
                        return;
                    }
                }

                if (TagAlreadyExists == false)
                {
                    MarkdownEditor markdownEditor = new MarkdownEditor(this, openFileDialog.FileName.ToString());
                    markdownEditor.Tag = slug;
                    markdownEditor.Show(AppDock);

                    // Disable the save
                    saveToolStripMenuItem.Enabled = false;
                }
            } else
            {
                // [TODO]: Tell the user that the file opening has been aborted
                logManager.LogInfo("File opening aborted by user.");
            }
        }

        public string GetSanitizedSlug(string RAW)
        {
            return Regex.Replace(RAW.Replace(" ", "-").ToLower().ToString(), "[^A-Za-z0-9\\--]", string.Empty).Replace("-", "_"); ;
        }

        public void SetAppStatus(string Message)
        {
            lblAppStatus.Text = Message;
            tmrStatusMessages.Start();
        }

        #endregion

        #region Exiting

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Save the settings before termination
            settingsManager.SaveFile(appSettings);
        }



        #endregion
    }
}
