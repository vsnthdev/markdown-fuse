﻿using System.IO;

namespace Markdown_Fuse.CustomClasses
{
    public class DirectoryManager : IDEFuse.DirectoryManager.DirectoryManager
    {
        #region Custom Properties

        // This will return the documents directory
        public string DocumentDirectory { get; internal set; }

        #endregion

        #region Initialization

        public DirectoryManager(string AppName)
        {
            // Pass the AppName property to the parent class
            this.AppName = AppName;

            // Start the parent class
            Initialize();

            // If running for first time, then create the documents folder
            if (FirstLaunch == true)
            {
                // Set the property values for the folders
                DocumentDirectory = Path.Combine(PersonalDirectory, "Documents");

                // Create the following directories
                Directory.CreateDirectory(DocumentDirectory);
            }
        }

        #endregion
    }
}
