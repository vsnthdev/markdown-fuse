﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Windows.Forms;

namespace Markdown_Fuse.CustomClasses
{
    public class SettingsManager : IDEFuse.Settings.SettingsManager
    {
        #region Custom Variables

        private IDEFuse.LogManager.LogManager logManager;

        #endregion

        #region Initialization

        public SettingsManager(string ENC_KEY, IDEFuse.LogManager.LogManager logManager)
        {
            // Set the properties
            this.ENC_KEY = ENC_KEY;
            this.logManager = logManager;

            // Initialize the SettingsManager for work
            InitializeManager();
        }

        #endregion

        #region Custom Methods

        // This method will take a preferences.config file and return an AppSettings CustomClass
        public AppSettings LoadFile(string PreferencesFilePath)
        {
            // Set the PreferencesFilePath property
            this.PreferencesFilePath = PreferencesFilePath;

            // Read the configuration file
            string _E;
            try
            {
                _E = File.ReadAllText(PreferencesFilePath);
            } catch
            {
                logManager.LogError("Could not load the preferences. File not found.");
                return new AppSettings();
            }

            // Decrypt it
            string _D;
            try
            {
                _D = _d(_E);
            } catch(Exception e)
            {
                // Log the exception message and return a new object
                logManager.LogError(e.Message.ToString());
                return new AppSettings();
            }

            // Parse JSON and return the object class
            return JsonConvert.DeserializeObject<AppSettings>(_D);
        }

        // This method will take an instance of appSettings and save it as an encrypted preferences file
        public void SaveFile(AppSettings appSettings)
        {
            // Parse the given class and get a JSON string
            string _J = JsonConvert.SerializeObject(appSettings, Formatting.Indented);

            // Encrypt it
            string _E = _e(_J);

            // Write the file back
            File.WriteAllText(PreferencesFilePath, _E);
        }

        #endregion
    }
}
