﻿using IDEFuse.Settings;

namespace Markdown_Fuse.CustomClasses
{
    public class AppSettings : appSettings
    {
        //
        // [INFO]: This class is operated by SettingsManager class. It exports all the preferences into preferences.config and
        // Allows the program to modify certain values on runtime.
        // This class has been inherited from IDEFuse.Settings.appSettings
        //

        #region Public Preferences

        // In which encoding to save the Markdown Files
        public string FileEncoding { get; set; } = "UTF-8";

        // Weather to close the StartPage once a markdown is loaded or not
        public bool CloseStartPageAfterLoaded { get; set; } = true;

        // Weather to show the tool bar and show status bar or not
        public bool ShowToolbar { get; set; } = true;
        public bool ShowStatusbar { get; set; } = true;

        #endregion
    }
}
