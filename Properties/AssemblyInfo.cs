﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Markdown Fuse")]
[assembly: AssemblyDescription("A Full Featured Markdown Editor For Windows.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fuse Apps")]
[assembly: AssemblyProduct("Markdown Fuse")]
[assembly: AssemblyCopyright("Copyright © Fuse Apps. All Rights Reserved.")]
[assembly: AssemblyTrademark("Fuse Apps logo and Vasanth Developer logo")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("38e3b4da-a9f5-4516-9836-0a9716efb9c1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("19.1.0.0")]
[assembly: AssemblyFileVersion("19.1.0.0")]
[assembly: NeutralResourcesLanguage("en-IN")]

