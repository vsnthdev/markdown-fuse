﻿using IDEFuse.DirectoryManager;
using IDEFuse.Settings;
using System;
using System.IO;
using System.Windows.Forms;

namespace Markdown_Fuse
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            // Start the mainWindow
            Application.Run(new MainWindow());
        }
    }
}
